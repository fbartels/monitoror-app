#!/bin/bash

if [ ! -e .env ]; then
	cat <<-EOF > "./.env"
# Config file to load
MO_CONFIG=/app/data/config.json
EOF
fi

source .env

if [ ! -e $MO_CONFIG ]; then
	cat <<-EOF > "$MO_CONFIG"
{
  "version": "2.0",
  "columns": 2,
  "tiles": [
    {
      "type": "PING",
      "label": "Localhost",
      "params": { "hostname": "127.0.0.1" }
    },
    {
      "type": "PORT",
      "label": "Dev server",
      "params": { "hostname": "127.0.0.1", "port": 8080 }
    },
    {
      "type": "HTTP-FORMATTED",
      "label": "Cloudron Version",
      "params": {
        "url": "$CLOUDRON_API_ORIGIN/api/v1/cloudron/status",
        "format": "JSON",
        "key": ".version",
        "regex": ".*"
      }
    }
  ]
}
EOF
fi

exec /usr/local/bin/monitoror
