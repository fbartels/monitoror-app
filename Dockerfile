FROM cloudron/base:1.0.0@sha256:147a648a068a2e746644746bbfb42eb7a50d682437cead3c67c933c546357617

EXPOSE 8080

ARG MONITORORVERSION=4.0.0
RUN curl -sL -o /usr/local/bin/monitoror https://github.com/monitoror/monitoror/releases/download/$MONITORORVERSION/monitoror-linux-amd64-$MONITORORVERSION && \
	chmod +x /usr/local/bin/monitoror && \
	monitoror --version

COPY start.sh /app/pkg/

WORKDIR /app/data

CMD [ "/app/pkg/start.sh" ]
