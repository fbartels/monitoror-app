clickable tiles: https://github.com/monitoror/monitoror/issues/259

specify config file at runtime (instead of having to pass it as url param): https://github.com/monitoror/monitoror/pull/318/files

example: https://monitor.9wd.eu/?configPath=./config.json
